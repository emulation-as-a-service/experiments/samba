from dperson/samba

run apk --no-cache add curl unzip fuse
run curl -O https://downloads.rclone.org/rclone-current-linux-amd64.zip
run unzip rclone-current-linux-amd64.zip
run cp rclone-*-linux-amd64/rclone /usr/bin

copy rclone-mount /usr/bin 
run chmod +x /usr/bin/rclone-mount

copy run.sh /usr/bin
run chmod +x /usr/bin/run.sh

entrypoint ["/sbin/tini", "--", "/usr/bin/run.sh"]
