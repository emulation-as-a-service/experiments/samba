#!/bin/sh
set -eu
set -x

export EAAS_STORAGE_CONFIG="$(cat /input/rclone.json)"
/usr/bin/rclone-mount 

/usr/bin/samba.sh "$@"